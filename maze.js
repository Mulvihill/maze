const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

function initMap () {
    for (row in map) {
        const getMapBody = document.getElementById('gameMap');
        const initRow = document.createElement('div');
        initRow.id = 'row';
        initRow.dataset.rowCoordinate = row;
        getMapBody.appendChild(initRow);
        for (cell in map[row]) {
            const newCell = document.createElement('div');
            newCell.dataset.cellCoordinate = row + "-" + cell;
            newCell.id = 'block';
            newCell.classList.add(cellType(map[row][cell]));
            initRow.appendChild(newCell);
        }
    }
};

function cellType(mapStr) {
    if (mapStr === " ") {
        var type = 'empty';
    }
    else if (mapStr === "W") {
        var type = 'wall';
    }
    else if (mapStr === "S") {
        var type = 'player';
    }
    else if (mapStr === "F") {
        var type = 'finish'
    }
    return type;
}


initMap();